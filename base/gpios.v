`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 08/04/2022 02:44:11 PM
// Design Name: 
// Module Name: gpios
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module gpios(output [4:0] buttons, input [7:0] leds
,input [4:0] buttonpins
,output [7:0] ledpins
    );
assign ledpins=leds;
assign buttons=buttonpins;
    
endmodule
